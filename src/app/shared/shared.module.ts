import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { MatInputModule, MatButtonModule, MatCheckboxModule, MatIconModule,
   MatDividerModule, MatFormFieldModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EnumArrayPipe } from './enum-array.pipe';

@NgModule({
  declarations: [EnumArrayPipe],
  exports: [
    CommonModule,
    NgbModule,
    TranslateModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,  // unused so far
    MatIconModule,  // unused so far
    BrowserAnimationsModule,  // unused so far
    MatDividerModule, // unused so far
    MatFormFieldModule,
    MatSelectModule,
    EnumArrayPipe
  ],
})
export class SharedModule { }
