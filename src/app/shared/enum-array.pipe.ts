import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToArray'
})
export class EnumArrayPipe implements PipeTransform {
  transform(enumObj: Object) {
  return Object.values(enumObj);
  }
}
