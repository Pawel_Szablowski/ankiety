import { SheetFormats } from './models/sheetFormats';
import { Component } from '@angular/core';
import * as XLSX from 'xlsx';
import { SheetsService } from './services/sheets.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuestionarreTypeEnum, Questionarre } from './models/questioniarre';

type AOA = any[][];
export class Person {
  id: number;
  name: String;
  surname: String;
  age: number;
}

export const PERSONS: Person[] = [
  {
    id: 1,
    name: 'Thomas',
    surname: 'NovickyŁĄŻĆŹąłóńę',
    age: 21
  },
  {
    id: 2,
    name: 'Adam',
    surname: 'Tracz',
    age: 12
  },
  {
    id: 3,
    name: 'Steve',
    surname: 'Laski',
    age: 38
  }
];

@Component({
  selector: 'app-sheets',
  templateUrl: './sheets.component.html',
  styleUrls: ['./sheets.component.scss']
})
export class SheetsComponent {
  data: AOA;
  rowsFromSheet: Array<Questionarre>;
  questionarreTypeEnum = QuestionarreTypeEnum;
  formFromSheet: FormGroup;
  sheetFormats = SheetFormats;

  constructor(
    private readonly sheets: SheetsService,
    private formBuilder: FormBuilder
  ) {}

  public saveSheet() {
    this.sheets.exportAsSheetsFile(PERSONS);
  }

  public readFile(evt: any) {
    const target: DataTransfer = <DataTransfer>evt.target;
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      /* save data */
      this.data = XLSX.utils.sheet_to_json(ws, { header: 1 });
      this.createFormFromSheet(this.data);
    };
    reader.readAsBinaryString(target.files[0]);
  }

  private createFormFromSheet(sheet: AOA) {
    this.clearForm();
    this.rowsFromSheet = sheet.map((arr) => {
      return {
        question: arr[0],
        answers: arr.slice(1),
        value: '',
        type:
          arr.length > 1
            ? QuestionarreTypeEnum.SELECT
            : QuestionarreTypeEnum.TEXTAREA
      };
    });

    this.formFromSheet = this.formBuilder.group(this.rowsFromSheet.map(row => row.value));
    this.rowsFromSheet.forEach((value, index) => {
      this.formFromSheet.controls[index].setValidators([Validators.required]);
    });
  }

  private clearForm() {}

  public formFromSheetSubmit() {
    console.log(this.formFromSheet)
  }


}
