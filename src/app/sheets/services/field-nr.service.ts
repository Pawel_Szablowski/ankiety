import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FieldNrService {
  subject = new Subject<number>();

  constructor() {}

  emitValue(value: number) {
    this.subject.next(value);
  }

  getSubject() {
    return this.subject;
  }
}
