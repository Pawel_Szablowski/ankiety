import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import { SheetFormats } from '../models/sheetFormats';

@Injectable({
  providedIn: 'root'
})
export class SheetsService {
  static toExportFileName(sheetsFileName: string, fileFormat?: SheetFormats): string {
    return `${sheetsFileName}${fileFormat ? fileFormat : '.xlsx'}`;
  }

  public exportAsSheetsFile(json: any[]): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ['data']
    };
    return XLSX.writeFile(
      workbook,
      SheetsService.toExportFileName(this.randomName())
    );
  }

  public readFile(path: any, options?: object) {
    XLSX.read(path, options);
  }

  // Todo: dodac potem zera do miesiaca i dnia
  private randomName() {
    const d = new Date();
    return `${d.getFullYear()}-${d.getMonth() +
      1}-${d.getDate()}_${Math.random()
      .toString(36)
      .substring(2)}`;
  }

  // TODO save form to DB
  public saveUserAnswers () {

  }

}
