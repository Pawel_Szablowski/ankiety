/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FieldNrService } from './field-nr.service';

describe('Service: FieldNr', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FieldNrService]
    });
  });

  it('should ...', inject([FieldNrService], (service: FieldNrService) => {
    expect(service).toBeTruthy();
  }));
});
