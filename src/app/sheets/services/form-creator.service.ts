import { CheckboxFieldComponent } from './../field-components/checkbox-field/checkbox-field.component';
import { FormField } from './../models/formField';
import { Injectable, Type } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormFieldTypes } from '../models/formFieldsTypes';
import { TextFieldComponent } from '../field-components/text-field/text-field.component';
import { TextareaFieldComponent } from '../field-components/textarea-field/textarea-field.component';

@Injectable({
  providedIn: 'root'
})
export class FormCreatorService {
  formFromSheet: FormGroup;
  constructor() { }

  getFormComponent(form: FormGroup, formComponentType: FormFieldTypes, label: string, nameIndex: number): FormField {
    let fieldComponent: Type<any>;
    switch (formComponentType) {
      case FormFieldTypes.CHECKBOX:
        fieldComponent = CheckboxFieldComponent;
        break;
      case FormFieldTypes.TEXT:
        fieldComponent = TextFieldComponent;
        break;
      case FormFieldTypes.TEXTAREA:
        fieldComponent = TextareaFieldComponent;
        break;
      default:
        fieldComponent = TextFieldComponent;
        break;
    }
    return new FormField(fieldComponent, {
      label,
      name: `name_${nameIndex}`,
      fieldNr: nameIndex
    }, form);
  }
}
