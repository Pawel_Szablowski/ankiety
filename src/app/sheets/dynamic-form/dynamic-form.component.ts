import { FieldNrService } from './../services/field-nr.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormFieldTypes } from './../models/formFieldsTypes';
import {
  Component,
  OnInit,
  ViewChild,
  ComponentFactoryResolver
} from '@angular/core';
import { DynamicFieldDirective } from '../directives/dynamic-field.directive';
import { FormFieldComponent } from '../models/formFieldsTypes';
import { FormCreatorService } from '../services/form-creator.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {
  @ViewChild(DynamicFieldDirective, { static: false })
  appDynamicField: DynamicFieldDirective;
  formFieldTypes = FormFieldTypes;
  dynamicForm: FormGroup;
  fieldNr: number;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private formCreatorService: FormCreatorService,
    private fb: FormBuilder,
    private fieldNrSubject: FieldNrService
  ) {}

  ngOnInit() {
    this.fieldNr = 0; // TODO - take last nr from existing fields or assign to 0
    this.dynamicForm = this.fb.group({});
    this.dynamicForm.valueChanges.subscribe(val => console.log(val));
    this.fieldNrSubject.getSubject().subscribe((fieldNr: number) => {
      console.log('subscribe', fieldNr);
      this.removeComponent(fieldNr);
      this.removeField(fieldNr);
    });
  }

  addComponent(formComponentType: FormFieldTypes, label: string) {

    const formField = this.formCreatorService.getFormComponent(
      this.dynamicForm,
      formComponentType,
      label,
      this.fieldNr
    );
    const dynamicFieldComponentFactory = this.componentFactoryResolver.resolveComponentFactory(
      formField.component
    );
    const viewContainerRef = this.appDynamicField.viewContainerRef;

    const dynamicFieldComponentRef = viewContainerRef.createComponent(
      dynamicFieldComponentFactory
    );

    (<FormFieldComponent>dynamicFieldComponentRef.instance).data =
      formField.data;
    (<FormFieldComponent>dynamicFieldComponentRef.instance).form =
      formField.form;
    // (<FormFieldComponent>dynamicFieldComponentRef.instance).fieldNr = this.fieldNr;
    this.dynamicForm.addControl(
      `name_${this.fieldNr}`,
      this.fb.control('', [Validators.required])
    );
    this.fieldNr++;
    console.log('dynamicform', this.dynamicForm);
    // todo - figure out how to make fieldNr reactive for view changes o know what control shouled be removed from form
  }

  removeComponent(fieldNr: number) {
    console.log('remove', this.appDynamicField.viewContainerRef);
    this.appDynamicField.viewContainerRef.remove(fieldNr);
  }

  removeField(fieldNr: number) {
    this.dynamicForm.removeControl(`name_${fieldNr}`);
    console.log(this.dynamicForm, 'dynamicForm');
  }

  onSubmit() {}
}
