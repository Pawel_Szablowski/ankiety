import { FormField } from './../../models/formField';
import { Component, OnInit, Input } from '@angular/core';
import { FormFieldComponent, FormFieldType } from '../../models/formFieldsTypes';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-checkbox-field',
  templateUrl: './checkbox-field.component.html',
  styleUrls: ['./checkbox-field.component.css']
})
export class CheckboxFieldComponent implements OnInit, FormFieldComponent {
  @Input() data: any;
  @Input() form: FormGroup;
  constructor( ) {}

  ngOnInit() {

  }

}
