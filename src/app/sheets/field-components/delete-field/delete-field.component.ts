import { Component, OnInit, Input } from '@angular/core';
import { FieldNrService } from '../../services/field-nr.service';

@Component({
  selector: 'app-delete-field',
  templateUrl: './delete-field.component.html',
  styleUrls: ['./delete-field.component.css']
})
export class DeleteFieldComponent implements OnInit {
  @Input() fieldNr: number;

  constructor(private fieldNrSubject: FieldNrService) {}

  ngOnInit() {}

  clear() {
    this.fieldNrSubject.emitValue(this.fieldNr);
    console.log('clear', this.fieldNr);
  }
}
