import { FormField } from './../../models/formField';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormFieldType } from '../../models/formFieldsTypes';

@Component({
  selector: 'app-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.css']
})
export class TextFieldComponent implements OnInit {
  @Input() data: any;
  @Input() form: FormGroup;
  constructor() {}

  ngOnInit() {}
}
