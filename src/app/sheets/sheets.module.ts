import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SheetsComponent } from './sheets.component';
import { SheetsService } from './services/sheets.service';
import { ReactiveFormsModule} from '@angular/forms';
import { TextareaFieldComponent } from './field-components/textarea-field/textarea-field.component';
import { TextFieldComponent } from './field-components/text-field/text-field.component';
import { DatepickerFieldComponent } from './field-components/datepicker-field/datepicker-field.component';
import { CheckboxFieldComponent } from './field-components/checkbox-field/checkbox-field.component';
import { SelectFieldComponent } from './field-components/select-field/select-field.component';
import { RadioFieldComponent } from './field-components/radio-field/radio-field.component';
import { DynamicFieldDirective } from './directives/dynamic-field.directive';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { DeleteFieldComponent } from './field-components/delete-field/delete-field.component';

@NgModule({
  declarations: [SheetsComponent, DynamicFieldDirective, DynamicFormComponent,
    TextareaFieldComponent, TextFieldComponent, DatepickerFieldComponent,
    CheckboxFieldComponent, SelectFieldComponent, RadioFieldComponent, DeleteFieldComponent],
  imports: [SharedModule, ReactiveFormsModule],
  entryComponents: [ TextareaFieldComponent, TextFieldComponent, DatepickerFieldComponent,
    CheckboxFieldComponent, SelectFieldComponent, RadioFieldComponent, DeleteFieldComponent],
  providers: [SheetsService],
  exports: [SheetsComponent]
})
export class SheetsModule {}
