import { FormGroup } from '@angular/forms';

export enum FormFieldTypes {
  COLOR = 'color',
  EMAIL = 'email',
  NUMBER = 'number',
  RADIO = 'radio',
  RANGE = 'range',
  RESET = 'reset',
  PHONE = 'tel',
  URL = 'url',
  TEXT = 'text',
  SELECT = 'select',
  CHECKBOX = 'checkbox',
  DATEPICKER = 'datepicker',
  TEXTAREA = 'textarea'
}


export interface FormFieldType {
  type?: FormFieldTypes;
  label: string;
  value?: string;
  name: string;
  fieldNr?: number;
}

export interface FormFieldComponent {
  data: any;
  form: FormGroup;
}

