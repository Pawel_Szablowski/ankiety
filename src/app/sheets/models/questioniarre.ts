export interface Questionarre {
  question: string;
  answers: Array<string>;
  type: QuestionarreTypeEnum;
  value: string;
}

export enum QuestionarreTypeEnum {
  SELECT = 'select',
  TEXTAREA = 'textarea'
}
