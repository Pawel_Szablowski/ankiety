import { Type } from '@angular/core';
import { FormFieldType } from './formFieldsTypes';
import { FormGroup } from '@angular/forms';

export class FormField {
  constructor(public component: Type<any>, public data: FormFieldType, public form: FormGroup) {}
}
