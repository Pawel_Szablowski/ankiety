export enum SheetFormats {
  XLS = '.xls',
  XLSX = '.xlsx',
  ODS = '.ods',
  XLSM = '.xlsm',
  XML = '.xml',
  XLSB = '.xlsb'
}
